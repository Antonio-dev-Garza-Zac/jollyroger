import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { PantallaComponent } from 'src/app/components/juego/pantalla.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RootComponent } from './components/root/root.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FormsModule} from '@angular/forms';
import { FrameworkModule} from '@next/nx-core';
import { CommonsModule} from '@next/nx-controls-common';
import { CommonModule } from '@angular/common';
import { HomeComponent } from 'src/app/components/home/home.component';


import {MatSelectModule,MatGridListModule, MatCardModule, MatInputModule, MatButtonModule, MatDialogModule, MatFormFieldModule ,MatTableModule}from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { ModalEmpledosComponent } from './components/modal-empledos/modal-empledos.component';
import { AuthenticationComponent } from './components/authentication/authentication.component';

const config = {
  usernameLabel: 'BRM',
  usernamePlaceholder: 'Usuario',
  endpoint: '',
  application: 'DESAPLICACIONREVERSA',
  applicationTitle: 'Desaplicacion de Pagos'
};


@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    PantallaComponent,
    HomeComponent,
    EmpleadosComponent,
    ModalEmpledosComponent,
    AuthenticationComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FrameworkModule.forRoot(config),
    CommonsModule.forRoot(),
    MatCardModule,
    MatGridListModule, 
    MatCardModule,
    MatInputModule, 
    MatButtonModule, 
    MatDialogModule, 
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
  ],
  entryComponents: [ModalEmpledosComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
