import { Component, OnInit,Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EmpleadosService, userInterface } from 'src/app/services/empleados.service'; 

@Component({
  selector: 'app-modal-empledos',
  templateUrl: './modal-empledos.component.html',
  styleUrls: ['./modal-empledos.component.scss'],
  providers: [EmpleadosService]
})

export class ModalEmpledosComponent implements OnInit {
  empleadoEdit: FormGroup;
  constructor(public dialogRef: MatDialogRef<ModalEmpledosComponent>,
    public empleadoService: EmpleadosService,
    public fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: userInterface) { }

  ngOnInit() {
    
    let puesto = (this.data.puesto === 'Desarrollador FrontEnd Sr') ? 1 : 2 ;
    this.empleadoEdit = this.fb.group({
      id: [''],
      nombre: ['', Validators.required],
      puesto: ['', Validators.required],
      foto: ['']
    })
    this.empleadoEdit.setValue({
      id: this.data.id,
      nombre: this.data.nombre,
      puesto: puesto,
      foto: this.data.foto
    })
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }


}
