import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalEmpledosComponent } from './modal-empledos.component';

describe('ModalEmpledosComponent', () => {
  let component: ModalEmpledosComponent;
  let fixture: ComponentFixture<ModalEmpledosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalEmpledosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalEmpledosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
