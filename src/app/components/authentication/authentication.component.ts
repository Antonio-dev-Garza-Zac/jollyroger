import { Component, OnInit ,ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SesionService } from 'src/app/services/sesion.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss'],
  providers: [SesionService]
})
export class AuthenticationComponent implements OnInit {
  @ViewChild('correo') correo: any;
  @ViewChild('password') pasword: any;

  infoUsuario: FormGroup;
  constructor(public fb: FormBuilder, private sesionService: SesionService, private router:Router) { }
 
  ngOnInit() {
    this.infoUsuario = this.fb.group({
      correo: ['', Validators.required],
      password: ['', Validators.required]
    });
    localStorage.clear();
  }

  iniciarSesion(){
    let data = {
      correo: this.correo.nativeElement.value,
      password: this.pasword.nativeElement.value,
    }
    if(data.correo != "" && data.password != ""){
      let res= this.sesionService.iniciarSesion(data);
      if(res != "no existe usuario"){
        this.sesionService.infoLocalUsuario(res);
        this.router.navigate(['home']);
      }else{
        alert("No existe el usuario");
      }
    }else{
      alert("Datos requeridos");
    }  
  }

}
