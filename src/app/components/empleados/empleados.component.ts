import { Component, OnInit ,ViewChild} from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { EmpleadosService,userInterface } from '../../services/empleados.service';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { ModalEmpledosComponent } from '../modal-empledos/modal-empledos.component';
import { AlertService, ConfirmationService } from '@next/nx-controls-common';
import { LoadingService } from '@next/nx-core';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.scss'],
  providers:[EmpleadosService]
})


export class EmpleadosComponent implements OnInit {
  @ViewChild('foto') myInputVariable: any;
  list: userInterface[] = [];
  brm: string;
  foto:String;
  nombre:string;
  puesto:string;
  registrarbtn:boolean =true;
  id:number = 0;

  displayedColumns: string[] = ['Brm', 'Nombre', 'Puesto', 'Foto','Acciones'];

  dataSource : any;

  formulario: FormGroup;
  

  constructor(private fb : FormBuilder,
    private empleadoService:EmpleadosService,
    public dialog: MatDialog,
    private alertService: AlertService,
    private confirmationService: ConfirmationService,
    private loading: LoadingService) { 
    
  }

  ngOnInit() {
    this.formulario = this.fb.group({
      brm: ['', [Validators.required]],
      foto: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      puesto: ['', [Validators.required]],
     
    });
    this.listarEmpleados();
  }

  public listarEmpleados(){
    this.loading.showLoading(true);
    this.empleadoService.getUsers().subscribe((res) => {
      this.list=<any>res;
      this.dataSource =  new MatTableDataSource<userInterface>(this.list);
      console.log("data source->",this.dataSource);
      console.log("lista->",this.list);
      this.loading.showLoading(false);
    },
    (error) => {
      console.error(error);
    });
  }

  registrar(){
    let puesto = (this.formulario.value.puesto == 1) ? "Desarrollador FrontEnd Sr." : "Desarrollador FrontEnd Jr." ;
    let data = {
      brm:this.brm,
      foto:puesto,
      nombre:this.nombre,
      puesto: this.foto
    }
   
    console.log("data->",data)
    if(data.brm != undefined && data.foto != undefined && data.puesto != undefined && data.nombre != undefined){
      this.empleadoService.addUser(data).subscribe(
        (res)=>{
          this.myInputVariable.nativeElement.value = "";
          this.listarEmpleados();
          this.formulario.reset();
          this.alertService.success('Empleado resgitrado con exito');
        },
        (err)=>{
          this.alertService.error('Error al registrar empleado');
          console.log("error->",err)
        }
      )
    }else{
      this.alertService.info('Todos los campos son requeridos');
    }
    
   
  }

  cargandoImagen(event:any){
    console.log("event->",event.target.files);
    this.foto=event.target.files[0].name;
	}

  openModalDialog(empleado:userInterface) {
    this.foto = empleado.foto;
    this.id = empleado.id;
    console.log(empleado.puesto);
    let puesto = (empleado.puesto === 'Desarrollador FrontEnd Sr.') ? 1 : 2 ;
    console.log(puesto)
    this.formulario.setValue({
      brm:empleado.brm,
      nombre: empleado.nombre,
      puesto: puesto,
      foto: ''
    });
    this.registrarbtn = false;   
  }

  actualizar(){
    let puestodata = (this.formulario.value.puesto == 1) ? "Desarrollador FrontEnd Sr." : "Desarrollador FrontEnd Jr." ;

    let data = {
      brm:this.brm,
      foto:this.foto,
      nombre:this.nombre,
      puesto: puestodata
    }
   
    //console.log("data->",data)
    if(data.brm != undefined && data.foto != undefined && data.puesto != undefined && data.nombre != undefined){
      this.empleadoService.udpateUser(this.id,data).subscribe(
        (res)=>{
          this.myInputVariable.nativeElement.value = "";
          this.listarEmpleados();
          this.formulario.reset();
          this.alertService.success('Empleado actualizado con exito');
          this.registrarbtn=true;
          
        },
        (err)=>{
          this.alertService.error('Error al actualizar empleado');
          console.log("error->",err)
        }
      )
    }else{
      this.alertService.info('Todos los campos son requeridos');
    }
  }
  
  cancelar(){
    this.formulario.reset();
    this.registrarbtn=true;
  }

  eliminar(empleado: any) {
    this.confirmationService.confirm({
      message: '¿Esta seguro de querer realizar esta acción?',
      lblOkBtn: 'Si',
      lblCancelBtn: 'Cancelar',
      accept: () => {
        this.empleadoService.deleteUser(empleado.id).subscribe((res) => {
          this.alertService.warn('Empleado eliminado correctamente');
          this.listarEmpleados();
        },
          (error) => {
            console.error(error);
          });
      }
    });
  }
  
}
