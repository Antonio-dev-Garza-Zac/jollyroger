import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from '@next/nx-controls-common';
import { Router } from '@angular/router';
import { LoadingService } from '@next/nx-core/services/loading.service';
import { SesionService } from 'src/app/services/sesion.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [SesionService]
})
export class HomeComponent implements OnInit {
 
  constructor(public confirmationService: ConfirmationService,
              public router: Router, public loginService: SesionService,
              public loadingService: LoadingService,
  ) {}

  title: string = "Web App"
  leftContent:any = [{description: 'Menú', isTitle: true }];

  usuario: any;
  mainContent:any = [
    { isHeader: false,
      content: [
        { content: 'Home', isTitle: true },
      ]
    }
  ];

 
  ngOnInit() {
    let infoUsuario = JSON.parse(localStorage.getItem('user'));
    console.log(infoUsuario);
    if(infoUsuario){
      this.usuario=infoUsuario.correo;
      if (infoUsuario.rol == "Jugador") {
        this.leftContent.push({ description: 'Juego', isTitle: false, route: 'home/jugador' });
      }else if (infoUsuario.rol == "Empleado") {
        this.leftContent.push({ description: 'Empleados', isTitle: false, route: 'home/empleado' });
      }else if(infoUsuario.rol == "Admin"){
        this.leftContent.push({ description: 'Empleados', isTitle: false, route: 'home/empleado' });
        this.leftContent.push({ description: 'Juego', isTitle: false, route: 'home/jugador' });
      }
    }else{
      this.router.navigate(['']);
    }
  }


  public logout() {
    this.loginService.cerrarSesion();
    this.router.navigate(['']);
  }
}

