import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { PantallaComponent } from './components/juego/pantalla.component';
import { AuthenticationComponent } from './components/authentication/authentication.component';
import { SesionGuard } from './guards/sesion.guard';


const routes: Routes = [
  { path: '', component: AuthenticationComponent },
  {
    path: 'home', component: HomeComponent,
    children: [
      { path: 'empleado', component: EmpleadosComponent },
      { path: 'jugador', component: PantallaComponent },
     // { path: 'login' , component: AuthenticationComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
