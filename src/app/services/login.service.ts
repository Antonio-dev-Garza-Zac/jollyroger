import { Injectable } from '@angular/core';

@Injectable()
export class LoginService {
  
  public iniciarSesion(usuario) {
    if(usuario.correo == "jesus@gmail.com" && usuario.password == "1234"){
      let data = {
        correo:"jesus@gmail.com",
        rol:"jugador",
        nombre:"jesus"
      };
      return data;
    }else if(usuario.correo == "Antonio@gmail.com" && usuario.password == "1234"){
      let data = {
        correo:"Antonio@gmail.com",
        rol:"Empleado",
        nombre:"jesus"
      };
      return data;
    }else if(usuario.correo == "Admin@gmail.com" && usuario.password == "1234"){
      let data = {
        correo:"Antonio@gmail.com",
        rol:"Admin",
        nombre:"jesus"
      };
      return data;
    }else{
      let data ="no existe usuario";
      return data;
    }
  }

  public infoLocalUsuario(user) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public cerrarSesion() {
    localStorage.clear();
  }

  public existeSesion() {
    return (localStorage.getItem('user') === null) ? false : true;
  }
}
