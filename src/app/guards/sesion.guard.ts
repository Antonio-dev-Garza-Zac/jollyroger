import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { SesionService } from '../services/sesion.service';

@Injectable({providedIn: "root"})
export class SesionGuard implements CanActivate {

  constructor(public authService: SesionService, public router: Router) {
  }

    canActivate() {
        if(this.authService.existeSesion()) {
            return true;
        }else{
            this.router.navigate(['']);
            return false;
        }
    }
}