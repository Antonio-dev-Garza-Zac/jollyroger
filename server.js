//Install express server
const express = require('express');
const path = require('path');

const app = express();

const port = process.env.PORT || 8080;
// Serve only the static files form the dist directory
app.use(express.static(path.join(__dirname, 'dist')));

app.get('/*', (req, res) => {
    res.send("hi (:");
    //res.sendFile('index.html', { root: 'dist/login/' }),
});

// Start the app by listening on the default Heroku port
app.listen(port, () => {
    console.log("listen in port->", port);
});